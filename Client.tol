/* -*- mode: c++ -*- */
/**
 * Client.tol --
 * 
 */

Text _.autodoc.member.Ping =
  "Verifica si un servidor RMTPS_SERVER esta ejecutandose en un "
  "ordenador remoto y esta escuchando peticiones."
  "Retorna 1 si el servidor esta ejecutandose o 0 en caso "
  "contrario";

/**
 *  Ping -- Verifica si un servidor RMTPS esta escuchando
 *
 *  Argumentos:
 *
 *    Host: host que aloja al servidor RMTPS
 *    
 *    Port: puerto donde el servidor RMPTS esta escuchando
 *
 *  Resultado:
 *  
 */
Real Ping(Text Host, Real Port)
{

  Set result = Tcl_Eval("::RmtPsClient::server_ping "+Host+" "+
                        FormatReal(Port, "%.0lf"));
  Real If(result["status"], Eval(result["result"]),{
      WriteLn("RmtPsClient::Ping: "+result["result"],"E");
      Real 0
    })
};

Text _.autodoc.member.Exec =
  "Ejecuta en un ordenador remoto un comando dado en CmdLine. "
  "CmdLine tambien contiene los argumentos separados por "
  "espacios. "
  "Retorna el PID del proceso ejecutado o 0 si algun error "
  "ocurrio durante el intento de ejecucion del comando. "
  "Necesita que un servidor RmtPS_Server este escuchando en el "
  "puerto Port del ordenador Host";
/**
 *  Exec -- Ejecuta un comando en remoto
 *
 *  Argumentos:
 *
 *    Host: host que aloja al servidor RMTPS
 *    
 *    Port: puerto donde el servidor RMPTS esta escuchando
 *
 *    CmdLine:
 *
 *  Resultado:
 *  
 */
Real Exec(Text Host, Real Port, Text CmdLine)
{

  Set result = Tcl_Eval("::RmtPsClient::ps_run "+Host+" "+
                        FormatReal(Port, "%.0lf")+
                        " {"+CmdLine+"}");
  Real If(result["status"], Eval(result["result"]),{
      WriteLn("RmtPsClient::Exec: "+result["result"],"E");
      Real 0
    })
};

Text _.autodoc.member.ExecTol =
  "Ejecuta en proceso TOL remoto. Args es un conjunto con los "
  "argumentos a pasar a TOL en la linea de comandos.\n"
  "Los elementos de Args son interpretados por el servidor de "
  "procesos TOL y se traducen en argumentos de linea de comandos "
  "al proceso TOL que se invoca. Los argumentos son:\n"
  "\tText project = nombre del proyecto que se cargara\n"
  "\tSet tolargs = conjunto de elementos a evaluar.\n"
  "El conjunto tolargs contiene expresiones TOL o archivos a "
  "evaluar en la linea de comandos. Cada uno de los "
  "elementos es un conjunto con los siguientes elementos con "
  "nombre:\n"
  "\tText item[1] : contiene la expresion o el nombre del "
  "\tarchivo a evaluar. El nombre de tolargs[1] debe ser expr o "
  "\tfile. Si el nombre es expr el valor se considera una "
  "\texpresion TOL. Si el nombre es file el valor se considera "
  "\tel nombre de un archivo a evaluar.\n"
  "\tText relative = nombre de proyecto respecto al cual se "
  "\tcargara el archivo TOL. Este campo es opcional y solo se "
  "\tconsidera cuando el item[1] es un archivo. Por omision "
  "\tel archivo se toma relativo al argumento project.\n\n"
  "Retorna el PID del proceso TOL ejecutado o 0 si algun error "
  "ocurrio durante el intento de ejecucion. "
  "Necesita que un servidor RmtPS_Server este escuchando en el "
  "puerto Port del ordenador Host"  ;

/**
 *  ExecTol -- Invoca un proceso TOL remoto
 *
 *  Argumentos:
 *
 *    Host: host que aloja al servidor RMTPS
 *    
 *    Port: puerto donde el servidor RMPTS esta escuchando
 *
 *    Args:
 *
 *  Resultado:
 *
 *    PID del proceso TOL lanzado o 0 si algun error ha ocurrido
 *  
 */
Real ExecTol(Text Host, Real Port, Set Args )
{

  Text cmd = "::RmtPsClient::ps_runtol";
  Text portNum = FormatReal(Port, "%.0lf");
  Text project = Args[ 1 ];
  Set tolargs = Args[ 2 ];
  Set cmdln_args = If( project=="", Copy( Empty ),
                       [[ "-project", project ]] );
  Set EvalSet( tolargs, Real( Set item ) {
      Text type = Name( item[ 1 ] );
      Case( type=="expr", {
          Set Append( cmdln_args, [[ "-expr", item[ 1 ] ]] );
          Real 0
        },
        type=="file", {
          Real idx_rel = FindIndexByName( item, "relative" );
          Set Append( cmdln_args,
                      If( idx_rel, {
                          Set [[ "-relative", item[ idx_rel ],
                                 "-file", item[ 1 ] ]]
                        }, {
                          Set [[ "-file", item[ 1 ] ]]
                        } ) );
          Real 0
        } ,
        type=="disable", {
          Set Append( cmdln_args, [[ "-" + item[ 1 ], "off" ]] );
          Real 0  
        },
        type=="enable", {
          Set Append( cmdln_args, [[ "-" + item[ 1 ], "on" ]] );
          Real 0  
        },
        1, {
          Warning( "RemoteTOL: tipo de elemento desconocido '"
                   + type + "' debe "
                   "ser 'expr' o 'file'" );
          Real 0
        } );
        Real 0
    } );
  Set tcl_args = [[ cmd, Host, portNum ]] << [[ cmdln_args ]];
  //Set View( tcl_args, "Std" );
  Set result = Tcl_EvalEx( tcl_args );
  Real If(result["status"], Eval(result["result"]),{
      WriteLn("RmtPsClient::ExecTol: "+result["result"],"E");
      Real 0
    })
};

Text _.autodoc.member.ExecMms =
  "Ejecuta en proceso MMS remoto. Args es un conjunto con los "
  "argumentos que describen el proceso de  MMS a ejecutar.\n"
  "Los elementos de Args son interpretados por el servidor de "
  "procesos TOL/MMS y se traducen en argumentos de linea de comandos "
  "al proceso TOL que se invoca. Los miembros de Args se interpretan según el nombre\n"
  "que tengan:\n"
  "\t\"repositorio\" : Text con nombre del repositorio desde donde se cargará el objeto MMS.\n"
  "\tEste repositorio se asume relativo a la configuración de BMR en el servidor rmtpsd.\n"
  "\t\"project\" : Text con nombre del proyecto desde donde se cargará el archivo de inicio del\n"
  "\tproyecto. Este proyecto se asume relativo a la configuración de BPR en el servidor rmtpsd.\n"
  "\t\"expr\" : debe ser un Text que contiene una expresion TOL a evaluar.\n"
  "\t\"file\" : puede ser un Text o un Set, si es un Text contiene el nombre de un archivo\n"
  "\ttol a incluir relativo al proyecto, si es un Set este debe contener un miembro  Text"
  "\tcon nombre \"path\" y opcionalmente un miembro Text de nombre \"relative\" cuyo valor\n"
  "\treferencia un proyecto respecto al cual se localiza el archivo.\n"
  "\testimation : Text que contiene el nombre de una estimación a ejecutar."
  "\t\"disable\" o \"enable\" : Text que contiene el nombre de una opción tipo on/off.\n"
  "\tlas opciones reconocidas son redirectLog (el log de la estimación se redirecciona\n"
  "\thacia el directorio log del repositorio MMS con nombre start.<PID>.log).\n"
  "Retorna el PID del proceso TOL ejecutado o 0 si algun error "
  "ocurrio durante el intento de ejecucion. "
  "Necesita que un servidor RmtPS_Server este escuchando en el "
  "puerto Port del ordenador Host"  ;
/**
 *  ExecMms -- Invoca un proceso MMS remoto
 *
 *  Argumentos:
 *
 *    Host: host que aloja al servidor RMTPS
 *    
 *    Port: puerto donde el servidor RMPTS esta escuchando
 *
 *    Args:
 *
 *  Resultado:
 *
 *    PID del proceso MMS lanzado o 0 si algun error ha ocurrido
 *  
 */
Real ExecMms(Text Host, Real Port, Set Args )
{

  Text cmd = "::RmtPsClient::ps_runmms";
  Text portNum = FormatReal( Port, "%.0lf" );
  Real projectFound = 0;
  Real repositoryFound = 0;
  Set tcl_args = Copy( Empty );
  Set EvalSet( Args, Real( Anything item ) {
      Text itemType = Name( item );
      Text g = Grammar( item );
      //WriteLn( "item " << item );
      Anything Case( itemType=="expr", {
          If( g == "Text",
              Set Append( tcl_args, [[ "-expr", item ]] ),
              Warning( "argumento expr debe ser Text: " <<
                       g << " " << item ) );
          Real 0
        },
        itemType=="estimation", {
          If( g == "Text",
              Set Append( tcl_args, [[ "-estimation", item ]] ),
              Warning( "argumento estimation debe ser Text: " <<
                       g << " " << item ) );
          Real 0
        },
        itemType=="repository", {
          If( g == "Text", {
              Set Append( tcl_args, [[ "-repository", item ]] );
              Real repositoryFound := 1;
              0
            },
            Warning( "argumento repository debe ser Text: " <<
                     g << " " << item ) );
          Real 0
        },
        itemType=="project", {
          If( g == "Text", {
              Set Append( tcl_args, [[ "-project", item ]] );
              Real projectFound := 1;
              0
            },
            Warning( "argumento project debe ser Text: " <<
                     g << " " << item ) );
          Real 0
        },
        itemType=="file", {
          Case( g == "Text", {
              Set Append( tcl_args, [[ "-file", item ]] )
            },
            g == "Set", {
              Real idxName = FindIndexByName( item, "path" );
              If( idxName, {
                  Real idxRelative = FindIndexByName( item, "relative" );
                  If( idxRelative,
                      Append( tcl_args, [[ "-relative", item[ idxRelative ] ]] ) );
                  Append( tcl_args, [[ "-file", item[ idxName ] ]] )
                }, {
                  Error( "no se ha encontrado campo path en argumento file: " <<
                         item );
                  Real Stop
                } )
            }, {
              Warning( "argumento file debe ser Text o Set: " <<
                       g << " " << item )
            } );
          Real 0
        } ,
        itemType=="disable", {
          If( g == "Text",
              Set Append( tcl_args, [[ "-" + item, "off" ]] ),
              Warning( "argumento disable debe ser Text: " <<
                       g << " " << item ) );
          Real 0  
        },
        itemType=="enable", {
          If( g == "Text",
              Set Append( tcl_args, [[ "-" + item, "on" ]] ),
              Warning( "argumento enable debe ser Text: " <<
                       g << " " << item ) );
          Real 0  
        },
        1, {
          Warning( "ExecMms: tipo de elemento desconocido '"
                   + itemType + "' debe "
                   "ser 'expr', 'file', 'disable', 'enable' or 'suffixSave'" );
          Real 0
        } );
        Real 0
    } );
  Real If ( And( !projectFound, !repositoryFound ), {
      Error( "ExecMms: no se ha encontrado argumento project o repository en " << Args );
      Real Stop
    } );

  Set tcl_expr = 
    [[ cmd, Host, portNum ]] << tcl_args;
  Set result = Tcl_EvalEx( tcl_expr );
  Real If(result["status"], Eval(result["result"]),{
      Error( "RmtPsClient::ExecMms: "+result["result"] );
      Real 0
    })
};

Text _.autodoc.member.ExecCvs =
  "Ejecuta en proceso CVS remoto. Args es un conjunto con los "
  "argumentos a pasar a CVS en la linea de comandos.\n"
  "Los elementos de Args son interpretados por el servidor de "
  "procesos CVS y se traducen en argumentos de linea de comandos "
  "al proceso CVS que se invoca. Los argumentos son:\n"
  "\tText CvsOperation = comando CVS a ejecutar, puede tomar los valores checkout o update\n"
  "\tText Project = nombre del proyecto sobre el que se invocara la operación CVS\n"
  "\tText Args = argumentos específicos relativos CvsOperation.\n"
  "Retorna el PID del proceso CVS ejecutado o 0 si algun error "
  "ocurrio durante el intento de ejecucion. "
  "Necesita que un servidor RmtPS_Server este escuchando en el "
  "puerto Port del ordenador Host";

/**
 *  ExecCvs -- Invoca un proceso TOL remoto
 *
 *  Argumentos:
 *
 *    Host: host que aloja al servidor RMTPS
 *    
 *    Port: puerto donde el servidor RMPTS esta escuchando
 *
 *    Args:
 *
 *  Resultado:
 *
 *    PID del proceso TOL lanzado o 0 si algun error ha ocurrido
 *  
 */
Real ExecCvs( Text Host, Real Port, Text CvsOperation, Text Project,
              Set Args )
{

  Text cmd = "::RmtPsClient::ps_runcvs";
  Text portNum = FormatReal(Port, "%.0lf");
  Text prjPath = getOptArg( Args, "PrjPath", "" );
  // here we can check the values of CvsOperation
  Set cvs_args = [[ "-cvsop", CvsOperation, "-project", Project ]];
  If( prjPath != "", Append( cvs_args, [[ "-prjpath", prjPath ]] ) );
  Set tcl_args = [[ cmd, Host, portNum ]] << [[ cvs_args ]];
  //Set View( tcl_args, "Std" );
  Set result = Tcl_EvalEx( tcl_args );
  Real If(result["status"], Eval(result["result"]),{
      WriteLn("RmtPsClient::ExecCvs: "+result["result"],"E");
      Real 0
    })
};

Text _.autodoc.member.CheckPid =
  "Verifica si un proceso dado por su PID continua vivo en un "
  "ordenador remoto. "
  "Retorna 1 si el proceso esta en la lista de procesos activos, "
  "0 si el proceso no esta activo o -1 si hubo un error durante "
  "la operacion remota, por ejemplo: PID invalido. "
  "Necesita que un servidor RmtPS_Server este escuchando en el "
  "puerto Port del ordenador Host";

/**
 *  CheckPid -- Verifica, en remoto, si un proceso continua vivo
 *
 *  Argumentos:
 *
 *    Host: host que aloja al servidor RMTPS
 *    
 *    Port: puerto donde el servidor RMPTS esta escuchando
 *
 *    PID: pid del proceso a verificar
 *
 *  Resultado:
 *  
 */
Real CheckPid(Text Host, Real Port, Real PID)
{
  Set result = Tcl_Eval("::rmtps_client::ps_is_active "+Host+" "+
                        FormatReal(Port, "%.0lf")+
                        " " + FormatReal(PID, "%.0lf"));
  Real If(result["status"], {
      Real alive = Eval(result["result"]);
      If(Eq(alive,-1), {
          WriteLn("hubo un error en ::rmtps_server::on_ps_alive, "
                  "rmtps_client no lo esta reportando por ahora. "
                  "Mientras tanto puede chequear el log del servidor", "W");
          Real 0
        }, alive)
    }, {
      WriteLn("RmtPsClient::CheckPid: "+result["result"],"E");
      Real 0
    })
};

Text _.autodoc.member.Kill =
  "Termina la ejecucion de un proceso, dado por su PID, en un "
  "ordenador remoto. "
  "Retorna 1 si el proceso se pudo finalizar o 0 si ocurrio un "
  "error, por ejemplo: PID invalido, o no hay permisos "
  "suficientes. "
  "Necesita que un servidor RmtPS_Server este escuchando en el "
  "puerto Port del ordenador Host";

/**
 *  Kill -- Termina la ejecucion de un proceso activo
 *
 *  Argumentos:
 *
 *    Host: host que aloja al servidor RMTPS
 *    
 *    Port: puerto donde el servidor RMPTS esta escuchando
 *
 *    PID: pid del proceso a finalizar
 *
 *  Resultado:
 *  
 */
Real Kill(Text Host, Real Port, Real PID)
{
  Set result = Tcl_Eval("::rmtps_client::ps_kill "+Host+" "+
                        FormatReal(Port, "%.0lf")+
                         " " + FormatReal(PID, "%.0lf"));
  Real If(result["status"], {
      Real killed = Eval(result["result"]);
      If(Eq(killed,-1), {
          WriteLn("hubo un error en ::rmtps_server::on_ps_kill, "
                  "rmtps_client no lo esta reportando por ahora. "
                  "Mientras tanto puede chequear el log del servidor","W");
          Real 0
        }, killed)
    }, {
      WriteLn("RmtPsClient::Kill: "+result["result"],"E");
      Real 0
    })
};
